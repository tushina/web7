window.addEventListener("DOMContentLoaded", function () {
    const PRICE_1 = 1850; //С� � µ� Ѕ� ° � ї� µСЂ� І� ѕ� і� ѕ � °СЂ� ѕ� ј� °С‚� °
    const PRICE_2 = 2400;//С� � µ� Ѕ� ° � ІС‚� ѕСЂ� ѕ� і� ѕ � °СЂ� ѕ� ј� °С‚� °
    const PRICE_3 = 3000;//С� � µ� Ѕ� ° С‚� µСЂС‚СЊ� µ� і� ѕ � °СЂ� ѕ� ј� °С‚� °
    const EXTRA_PRICE = 0;
    const RADIO_1 = 1000; //� ІС‹� ±� ѕСЂ Сѓ � ІС‚� ѕСЂ� ѕ� і� ѕ � °СЂ� ѕ� ј� °С‚� °
    const RADIO_2 = 1500;
    const RADIO_3 = 500;
    const CHECKBOX = 350; //С‡� µ� є� ±� ѕ� єСЃ С‚СЂ� µС‚СЊ� µ� і� ѕ � °СЂ� ѕ� ј� °С‚� °
    let price = PRICE_1;
    let extraPrice = EXTRA_PRICE;
    let result;
    let calc = document.getElementById("calc"); 
    let myNum = document.getElementById("number"); //� ±� µСЂ� µС‚ � є� ѕ� »-� І� ѕ С‚� ѕ� І� °СЂ� °
    let resultSpan = document.getElementById("result");// 
    let select = document.getElementById("select");// � ѕ� їСЂ� µ� ґ� µ� »СЏ� µС‚ � є� °� є� ѕ� № С‚� ѕ� І� °СЂ � ІС‹� ±СЂ� °� »� ё
    let radiosDiv = document.getElementById("radio-div");// Сѓ � ІС‚� ѕСЂ� ѕ� і� ѕ С‚� ѕ� І� °СЂ� ° � ѕ� їСЂ� µ� ґ� µСЏ� µС‚ � І� ё� ґ , � І � є� ѕС‚� ѕСЂ� ѕ� ј � ІС‹� ±СЂ� °� »� ё
    let radios = document.querySelectorAll("#radio-div input[type=radio]");// � І� ѕ� ·� ІСЂ� °С‰� °� µС‚ � ї� µСЂ� ІС‹� № СЌ� »� µ� ј� µ� ЅС‚ ( Element ) � ґ� ѕ� єСѓ� ј� µ� ЅС‚� °, � є� ѕС‚� ѕСЂС‹� № СЃ� ѕ� ѕС‚� І� µС‚СЃС‚� ІСѓ� µС‚ Сѓ� є� °� ·� °� Ѕ� Ѕ� ѕ� јСѓ СЃ� µ� »� µ� єС‚� ѕСЂСѓ � ё� »� ё � іСЂСѓ� ї� ї� µ СЃ� µ� »� µ� єС‚� ѕСЂ� ѕ� І.
    let checkboxDiv = document.getElementById("check-div");// � ±� µСЂ� µС‚ Сѓ 3 СЌ� »� µ� ј� µ� ЅС‚� °
    let checkbox = document.getElementById("checkbox");// � ІС‹� ±СЂ� °� »� ё � »� ё � јС‹ � ѕС‚� єСЂС‹С‚� єСѓ 
    select.addEventListener("change", function (event) {// � ·� °� їСѓСЃ� є� °� µС‚ СЃ� ѕ� ±С‹С‚� ё� µ � ІС‹� ±� ѕСЂ � °СЂ� ѕ� ј� °С‚� °
        let option = event.target;// СЃСЃС‹� »� є� ° � Ѕ� ° � ѕ� ±СЉ� µ� єС‚, � є� ѕС‚� ѕСЂС‹� № СЋС‹� » � ё� Ѕ� ёС� � ё� °С‚� ѕСЂ� ѕ� ј СЃ� ѕ� ±С‹С‚� ёСЏ
        if (option.value === "1") {//
            radiosDiv.classList.add("d-none");//
            checkboxDiv.classList.add("d-none");//
            extraPrice = EXTRA_PRICE;//
            price = PRICE_1;//
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");//
            checkboxDiv.classList.add("d-none");//
            extraPrice = RADIO_1;//
            price = PRICE_2;//
            document.getElementById("radio1").checked = true;//
        }
        if (option.value === "3") {//
            checkboxDiv.classList.remove("d-none");//
            radiosDiv.classList.add("d-none");//
            extraPrice = EXTRA_PRICE;//
            price = PRICE_3;//
            checkbox.checked = false;//
        }
    });
    radios.forEach(function (currentRadio) {//
        currentRadio.addEventListener("change", function (event) {//
            let radio = event.target;//
            if (radio.value === "r1") {//
                extraPrice = RADIO_1;//
            }
            if (radio.value === "r2") {//
                extraPrice = RADIO_2;//
            }
            if (radio.value === "r3") {//
                extraPrice = RADIO_3;//
            }
        });
    });
    checkbox.addEventListener("change", function () {//
        if (checkbox.checked) {
            extraPrice = CHECKBOX;//
        } 
        else {
            extraPrice = EXTRA_PRICE;//
        }
    });
    calc.addEventListener("change", function () {//
        if (myNum.value < 1) {
            myNum.value = 1;
        }
        result = (price + extraPrice) * myNum.value;//
        resultSpan.innerHTML = result;//
    });
});



